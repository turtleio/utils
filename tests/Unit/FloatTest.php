<?php

namespace Ifirlana\Utils\Tests\Unit;

use Ifirlana\Utils\Helpers;

class FloatTest extends \Orchestra\Testbench\TestCase {

    /** @test */
      public function helper_float_success() {

        $helper = new Helpers;
        $result = $helper->id_float(20.11);
        print_r($result);

        if ($result == "20,11") {
            $this->assertTrue(true);
        }else {
            $this->assertTrue(false);
        }
    }

    /** @test */
    public function helper_float_from_int_success() {

        $helper = new Helpers;
        $result = $helper->id_float(20);
        print_r($result);

        if ($result == "20") {
            $this->assertTrue(true);
        }else {
            $this->assertTrue(false);
        }
    }

    /** @test */
    public function helper_currency_handle_string_format() {

        try {

            $helper = new Helpers;
            $result = $helper->id_float("OK20.00");
            print_r($result);
            $this->assertTrue(false);
    
        } catch (\Throwable $th) {
            //throw $th;
            print_r($th->getMessage());
            $this->assertTrue(true);
        }

    }
}