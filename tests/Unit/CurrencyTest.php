<?php

namespace Ifirlana\Utils\Tests\Unit;

use Ifirlana\Utils\Helpers;

class CurrencyTest extends \Orchestra\Testbench\TestCase {

    /** @test */
    public function helper_currency_success() {

        $helper = new Helpers;
        $result = $helper->id_currency_format(20000);
        print_r($result);

        if ($result == "Rp20.000") {
            $this->assertTrue(true);
        }else {
            $this->assertTrue(false);
        }
    }
    /** @test */
    public function helper_currency_still_formatted() {

        $helper = new Helpers;
        $result = $helper->id_currency_format(20000.50);
        print_r($result);

        if ($result == "Rp20.000") {
            $this->assertTrue(true);
        }else {
            $this->assertTrue(false);
        }
    }

    /** @test */
    public function helper_currency_cannot_handle_mixingstring() {

        try {

            $helper = new Helpers;
            $result = $helper->id_currency_format("OK20000");
            print_r($result);
            $this->assertTrue(false);
    
        } catch (\Throwable $th) {
            //throw $th;
            print_r($th->getMessage());
            $this->assertTrue(true);
        }

    }

    /** @test */
    public function helper_currency_handle_string_format() {

        try {

            $helper = new Helpers;
            $result = $helper->id_currency_format("20000.00");
            print_r($result);
            $this->assertTrue(true);
    
        } catch (\Throwable $th) {
            //throw $th;
            print_r($th->getMessage());
            $this->assertTrue(false);
        }

    }

}
