<?php

namespace Ifirlana\Utils;

class Helpers {
    
    function id_currency_format(int $number, $with_rp=true) {
        $ret = number_format($number, 0, ',','.');
        if($with_rp) {
            $ret = "Rp" . $ret;
        }
        return $ret;
    }

    function id_float(float $float_val) {
        return str_replace(".", ",", $float_val);
    }
}